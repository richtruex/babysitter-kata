// Because we're dealing with night, we're going to do something a bit weird.
// Everything after midnight will be increased by 12, like military time, but starting at noon.
function calculatePay(start, bedtime, end){
	// This is *not* a constant.  Instead it acts as an additional input that is always 12.
	midnight = 12;

	const beforeBedtimePay = 12;
	const afterBedtimePay = 8;
	const afterMidnightPay = 16;


	// Sanity checks.  These first two should not be possible from the web page unless someone is deliberately breaking things.
	if (start < 5) {
		return "Start Time cannot be earlier than 5 pm.";
	}

	if (end > 16) {
		return "End Time cannot be later than 4 am.";
	}

	if (end < start) {
		return "End Time must be later than start time.";
	}


	// Account for unusual start and end times.
	// With these, the sections of the night will always be start->bedtime, bedtime->midnight, and midnight->end.
	// Any that don't apply will calculate as zero.
	if (start > bedtime) {
		// Pretend bedtime was when we started.
		bedtime = start;
	}

	if (start > midnight) {
		// Replace midnight with start time.
		midnight = start;
	}

	if (end < midnight) {
		// Bedtime->midnight will act like bedtime->end.
		midnight = end;
	}

	// Bedtime becomes either midnight or start time, whichever is greater.
	if (bedtime > midnight){
		// Charge the same as any time after midnight;
		bedtime = midnight;
	}


	// Calculate total from parts.
	result = 0;
	// If any of these have been made equal above, the corresponding amount will be will be zero.
	result += (bedtime - start) * beforeBedtimePay;
	result += (midnight - bedtime) * afterBedtimePay;
	result += (end - midnight) * afterMidnightPay;

	return result;
}