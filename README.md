This is a coding project I completed for a previous job interview.  I've left it up as an example of my coding style and ability.  (The vast majority of my code is proprietary and therefore not something I can share.)

The goal was to create a tool for a babysitter to use to determine the total cost of a babysitting session.

Prices were $12/hour before bedtime, $8/hour after bedtime, and $16/hour after midnight.

The instructions stipulated that UI was not important - I can certainly do better than this.

The choice of language, platform, and tools (e.g. jQuery and qUnit) was mine.

==========================

How to build and run:

There's no need to build.  Just open index.html in a web browser.  To run the tests, just open tests.html, or follow the link from index.html.


Assumptions:

The instructions say to bill in whole hour increments, but don't address partial hours in the subsections.  For example, an evening starting at 6:00, putting the child to bed at 7:30, and leaving at 9:00.  Should that be 2 hours at the pre-bedtime rate and 1 hour at the after bedtime rate, or one and a half hours in each?  Either way is still technically billing in whole hours.  To avoid this, I have restricted the system to having each time (start, bedtime, and end) on an hour.

There are no restrictions listed for bedtime, but I assume that if bedtime is later than midnight, the after-midnight rate applies.

